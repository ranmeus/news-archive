FROM python:3.8-alpine
WORKDIR /code
COPY requirements.txt /code/
COPY app.py /code/
COPY wait_for /code/
RUN chmod +x wait_for
RUN pip install -r requirements.txt --no-cache-dir
CMD ["sh", "-c", "./wait_for db:3306 -- python3 app.py"]
 