# NEWS-ARCHIVE #

This repository contains a docker compose file to create Docker applications.

There are two main applications, the MySQL database _db_ to persists the data, and the python program _news_ to retrieve the required data.

## DB

For the DB the _mysql:8_ image from the docker hub is used. 
The password to connect and to manipulate the DB can be found in _password.txt_.
The port _3306_ is specified to connect to the DB for further analysis.
To persist the content beyond the lifecycle of the DB data is persisted to _/opt/mysql_data_ on the host machine.

The DB has one table _news_, which contains five columns: _id, title, sub_title, abstract, download_time, update_time_.
The _title_ is a unique string. _download_time_ stores the time when the news was read for the first time.
The _update_time_ saves the time when the news was read for the last time.


## NEWS

This is a python application running in the Docker container _python:3.8-alpine_.
It looks at the international page of [www.spiegel.de](https://www.spiegel.de/international/)
and extracts the news and save them into the DB.

To simplify the process, it is assumed the title is always unique and will not change overtime.
That is needed to determine the update time of an article. Different titles are therefore different articles.

To achieve this goal following libraries are installed during the building of the docker file automatically:

 - _bs4_ to handle the html document
 - _schedule_ to set a repeating timer
 - _mysql-connector-python_ to communicate with the mysql DB
 
 Those libraries are also listed in the _requirements.txt_ file.

As default the application will read only the main page every 15 minutes, and extracts the needed data.
To read more pages or to change the reading rate, following evironment variables can be changed in the _docker-compose.yaml_ file:

 - _PAGE_AMOUNT_ is the number of pages to be read
 - _CRAWL_TIMER_ is the time between each complete reading
 - _PAGE_TIMER_ is the time between the reading of each page
 
However, if the time needed for a complete reading is higher than the defined _CRAWL_TIMER_,
the first one will be used as _CRAWL_TIMER_ instead.
 
## How to start the application
 
Inside the folder of the cloned repository, run the following command:
 
```
docker-compose up
```
 
## DB Access

To inspect the saved data in the DB, any SQL client with MySQL support can be used. 
 
 - Host: db (or IP of the host system)
 - Database: mydb
 - Port: 3306
 - Username: root
 - Password: acrazypwd
