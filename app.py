import bs4 as soup
import urllib.request as ureq
import urllib.error as uerr
import schedule
import time
import datetime
import mysql.connector as sql
import os
from pathlib import Path
from getpass import getpass


class DBManager:
    def __init__(self,
                 database='mydb',
                 host='db',
                 user='root',
                 password_file=None):
        existsFile = Path(password_file).is_file()
        if existsFile:
            pf = open(password_file, 'r')
            pwd = pf.read()
            pf.close()
        else:
            host = 'localhost'
        while True:
            try:
                if not existsFile:
                    print("Running in manual mode!")
                    pwd = getpass()
                self.connection = sql.connect(
                    user=user,
                    password=pwd,
                    host=host,
                    database=database,
                    auth_plugin='mysql_native_password'
                )
                break
            except sql.Error as e:
                print('Error:', e)
                if e.errno == 1045:
                    print("Wrong password, retry!")
                else:
                    print('The database is not reachable. Retry in 30s!')
                    time.sleep(30)
        self.cursor = self.connection.cursor()

    def create_table(self):
        while True:
            try:
                self.cursor.execute('CREATE TABLE IF NOT EXISTS news (\
                    id INT AUTO_INCREMENT PRIMARY KEY, \
                    title VARCHAR(255) UNIQUE, \
                    sub_title VARCHAR(255), \
                    abstract TEXT, \
                    download_time DATETIME, \
                    update_time DATETIME\
                    )')
                break
            except sql.Error as e:
                print('Error:', e)
                print('Table news cannot be created. Retry in 5s!')
                time.sleep(5)
        self.connection.commit()

    def insert(self, title, subtitle, abstract, now):
        try:
            self.cursor.execute(
                'INSERT INTO news (title, sub_title, abstract, download_time, update_time) \
                VALUES (%s, %s, %s, %s, %s) \
                ON DUPLICATE KEY UPDATE \
                sub_title=%s,abstract=%s,update_time=%s',
                (title, subtitle, abstract, now, now, subtitle, abstract, now))
        except sql.Error as e:
            print('Error:', e)
            return False
        self.connection.commit()
        return True

    # for testing purpose
    def query_news(self):
        print('query news')
        self.cursor.execute('SELECT title, download_time, update_time \
                            FROM news')
        res = self.cursor.fetchall()
        for r in res:
            print('download time %s' % r[1].strftime('%d/%m/%Y-%H:%M:%S'),
                  'update time %s' % r[2].strftime('%d/%m/%Y-%H:%M:%S'),
                  sep=', ')
            print(r[0])


def crawl():
    url = 'https://www.spiegel.de/international/'
    purl = 'https://www.spiegel.de/international/p%s/'

    crawl_page(url)
    pNr = 2
    while pNr <= pageAmount:
        time.sleep(pageTimer * 60)
        if not crawl_page(purl % pNr):
            break
        pNr += 1


def crawl_page(url):
    print('\ncrawl ' + url)
    ts = time.time()
    now = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    try:
        sauce = ureq.urlopen(url).read()
    except uerr.URLError as e:
        print('Error:', e)
        print('Open the URL _%s_ is not possible. \
        Current cycle is suspended' % url)
        return False

    souppage = soup.BeautifulSoup(sauce, 'html.parser')
    articles = souppage.find_all('article', {'aria-label': True})

    if len(articles) == 0:
        print('URL _%s_ do not have any article. \
              Current cycle is suspended' % url)
        return False

    for article in articles:
        title = article['aria-label'].strip()
        subtitle = article.find('span',
                                {'class': 'text-primary-base'}).string.strip()
        aobj = article.find('span', {'class': 'leading-loose'})
        abstract = ''
        if aobj is not None:
            abstract = aobj.string.strip()
        print('\nInsert: ' + title + '\n\t' + subtitle + '\n' + abstract)
        if not conn.insert(title, subtitle, abstract, now):
            print('Insert _%s_ not possible. \
                  Current cycle is suspended' % title)
            return False
    return True


try:
    pageAmount = int(os.getenv('PAGE_AMOUNT', 1))
    crawlTimer = int(os.getenv('CRAWL_TIMER', 15))
    pageTimer = int(os.getenv('PAGE_TIMER', 1))
except ValueError as e:
    print('Error:', e)
    print('Environment variables must be integer')
    exit()

crawlTimer = max(crawlTimer, pageAmount * pageTimer)
conn = DBManager(password_file='/run/secrets/db-password')
conn.create_table()
schedule.every(crawlTimer).minutes.do(crawl)

crawl()
while True:
    schedule.run_pending()
    time.sleep(60)
